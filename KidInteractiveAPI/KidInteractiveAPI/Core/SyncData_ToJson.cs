using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KidInteractive.Models;
using KidInteractiveAPI.Models;
using Newtonsoft.Json;

namespace KidInteractive
{
    public class SyncData_ToJson 
    {
        KidInteractiveContext _context = new KidInteractiveContext();
        public List<obj_Row> GetString_InserData(string table_name)
        {
            List<obj_Row> l_Json = new List<obj_Row>();
            obj_Row oTemp = null;
            if(table_name == "Answer".ToLower())
            {
                var list_obj = _context.Answer;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();                   
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES ("+ o.Id +", '"+ o.Content +"', "+ o.IsCorrect +", "+ o.Status +", "+ o.QuestionId +");";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetRoleClaims".ToLower())
            {
                var list_obj = _context.AspNetRoleClaims;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();        
                    oTemp.Value = "INSERT INTO " + table_name + " VALUES (" + o.Id + ", '" + o.RoleId + "', '" + o.ClaimType + "', '" + o.ClaimValue + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetRoles".ToLower())
            {
                var list_obj = _context.AspNetRoles;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();                        
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES ('" + o.Id + "', '" + o.Name + "', '" + o.NormalizedName + "', '" + o.ConcurrencyStamp + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetUserClaims".ToLower())
            {
                var list_obj = _context.AspNetUserClaims;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();   
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES (" + o.Id + ", '" + o.UserId + "', '" + o.ClaimType + "', '" + o.ClaimValue + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetUserLogins".ToLower())
            {
                var list_obj = _context.AspNetUserLogins;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();                      
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES ('" + o.LoginProvider + "', '" + o.ProviderKey + "', '" + o.ProviderDisplayName + "', '" + o.UserId + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetUserRoles".ToLower())
            {
                var list_obj = _context.AspNetUserRoles;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();    
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES ('" + o.UserId + "', '" + o.RoleId + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetUsers".ToLower())
            {
                var list_obj = _context.AspNetUsers;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();    
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES ('" + o.Id + "', '" + o.UserName + "', '" + o.NormalizedUserName + "', '" + o.Email + "', '" + o.NormalizedEmail + "', "  + o.EmailConfirmed + 
                    ",'"+ o.PasswordHash +"','"+ o.SecurityStamp +"','"+ o.ConcurrencyStamp +"','"+ o.PhoneNumber +"',"+ o.PhoneNumberConfirmed +","+ o.TwoFactorEnabled +",'"+ o.LockoutEnd +"',"+ o.LockoutEnabled +","+ o.AccessFailedCount +");";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "AspNetUserTokens".ToLower())
            {
                var list_obj = _context.AspNetUserTokens;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES ('" + o.UserId + "', '" + o.LoginProvider + "', '" + o.Name + "', '" + o.Value + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "Copyright".ToLower())
            {
                var list_obj = _context.Copyright;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();
                    string YearOfCopy = o.YearOfCopy == null ? "null" : o.YearOfCopy.ToString();
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES (" + o.Id + ", '" + o.CompanyName + "', " + YearOfCopy + ", '" + o.LicenseType + "', '" + o.Address + "', '" + o.Telephone + "', '" + o.Email + "');";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "Menu".ToLower())
            {
                var list_obj = _context.Menu;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();
                    string ParentId = o.ParentId == null ? "null" : o.ParentId.ToString();
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES (" + o.Id + ", '" + o.Title + "', '" + o.Description + "', '" + o.Icon + "', " + o.Status + ", '" + o.Link + "', " + o.LinkType + ", "+ ParentId + ");";                    
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "MultiChoice".ToLower())
            {
                var list_obj = _context.MultiChoice;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES (" + o.Id + ", '" + o.Topic + "', '" + o.MenuId + "', '" + o.CreatedDate + "', '" + o.StartDate + "', '" + o.EndDate + "', " + o.Status + ", '" + o.Position + "', " + o.QuestionType + ");";
                    l_Json.Add(oTemp);
                }
            }
            else if(table_name == "Question".ToLower())
            {
                var list_obj = _context.Question;
                foreach(var o in list_obj)
                {
                    oTemp = new obj_Row();
                    oTemp.Value = "INSERT INTO "+ table_name +" VALUES (" + o.Id + ", '" + o.Title + "', '" + o.Content + "', " + o.Status + ", " + o.Qtype + ", " + o.Point + ", " + o.QuestionGroupId + ");";
                    l_Json.Add(oTemp);
                }
            }            
            return l_Json;
        }
        public string GetData(obj_Table lObj1)
        {
            List<obj_Row> lResult = new List<obj_Row>();
            foreach(var o in lObj1.table_name)
            {
                lResult.AddRange(GetString_InserData(o.Value));
            }            
            return JsonConvert.SerializeObject(lResult);
        }
    
    }
}
