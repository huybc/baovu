﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KidInteractive.Models;
using KidInteractiveAPI.Models;
using Microsoft.EntityFrameworkCore;
using System.Runtime.InteropServices.WindowsRuntime;

namespace KidInteractive.Controllers
{
    public class MenuController : Controller
    {
        
        KidInteractiveContext _context = new KidInteractiveContext();
        private readonly ILogger<HomeController> _logger;

        public MenuController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult GetData()
        {
            var listData = _context.Menu.Include(o => o.Parent);           
            return Ok(listData);
        }

        public IActionResult GetDataMinified()
        {
            var list = _context.Menu.Select(x => new
            {
                x.Id,
                x.ParentId,
                x.Title,
                x.Link,
                x.LinkType,
                x.Status,
                x.Icon
            });
            return Ok(list);        
        }
        public IActionResult objGetData(long id)
        {
            var obj = _context.Menu.SingleOrDefault(o => o.Id == id);           
            return Ok(obj);
        }
        [HttpPost]
        public IActionResult InsertData([FromBody] Menu obj)
        {
            try
            {
                _context.Menu.Add(obj);
                _context.SaveChanges();
            }
            catch(Exception ex){ return Ok(false); }
            return Ok(true);
        }
                [HttpPost]
        public IActionResult UpdateData([FromBody] Menu obj)
        {
            try
            {
                _context.Menu.Update(obj);
                _context.SaveChanges();
            }
            catch(Exception ex){ return Ok(false); }
            return Ok(true);
        }
        public IActionResult DeleteData(long id)
        {
            try
            {
                var result = _context.Menu.Find(id);
                _context.Menu.Remove(result);
                _context.SaveChanges();             
            }
            catch(Exception ex){ return Ok(false); }
            return Ok(true);
        }
    }
}
