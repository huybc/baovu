﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using KidInteractive.Models;
using KidInteractiveAPI.Models;

namespace KidInteractive.Controllers
{
    public class CopyrightController : Controller
    {
        
        KidInteractiveContext _context = new KidInteractiveContext();
        private readonly ILogger<HomeController> _logger;

        public CopyrightController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult GetData()
        {
            var listData = _context.Copyright;           
            return Ok(listData);
        }
        [HttpPost]
        public IActionResult InsertData([FromBody] Copyright obj)
        {
            try
            {
                _context.Copyright.Add(obj);
                _context.SaveChanges();
            }
            catch(Exception ex){ return Ok(false); }
            return Ok(true);
        }
    }
}
