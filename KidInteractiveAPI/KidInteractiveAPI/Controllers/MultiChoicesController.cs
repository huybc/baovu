﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using KidInteractiveAPI.Models;

namespace KidInteractiveAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MultiChoicesController : ControllerBase
    {
        private readonly KidInteractiveContext _context;

        public MultiChoicesController(KidInteractiveContext context)
        {
            _context = context;
        }

        // GET: api/MultiChoices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MultiChoice>>> GetMultiChoice()
        {
            return await _context.MultiChoice.ToListAsync();
        }

        // GET: api/MultiChoices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MultiChoice>> GetMultiChoice(long id)
        {
            var multiChoice = await _context.MultiChoice.FindAsync(id);

            if (multiChoice == null)
            {
                return NotFound();
            }

            return multiChoice;
        }

        // PUT: api/MultiChoices/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMultiChoice(long id, MultiChoice multiChoice)
        {
            if (id != multiChoice.Id)
            {
                return BadRequest();
            }

            _context.Entry(multiChoice).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MultiChoiceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MultiChoices
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MultiChoice>> PostMultiChoice(MultiChoice multiChoice)
        {
            _context.MultiChoice.Add(multiChoice);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMultiChoice", new { id = multiChoice.Id }, multiChoice);
        }

        // DELETE: api/MultiChoices/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MultiChoice>> DeleteMultiChoice(long id)
        {
            var multiChoice = await _context.MultiChoice.FindAsync(id);
            if (multiChoice == null)
            {
                return NotFound();
            }

            _context.MultiChoice.Remove(multiChoice);
            await _context.SaveChangesAsync();

            return multiChoice;
        }

        private bool MultiChoiceExists(long id)
        {
            return _context.MultiChoice.Any(e => e.Id == id);
        }
    }
}
