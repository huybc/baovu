﻿using System;
using System.Collections.Generic;

namespace KidInteractiveAPI.Models
{
    public partial class MultiChoice
    {
        public MultiChoice()
        {
            Question = new HashSet<Question>();
        }

        public long Id { get; set; }
        public string Topic { get; set; }
        public long MenuId { get; set; }
        public string CreatedDate { get; set; } = DateTime.Now.ToString("YYYY-MM-DD");
        public string StartDate { get; set; } = DateTime.Now.ToString("YYYY-MM-DD");
        public string EndDate { get; set; } = DateTime.Now.ToString("YYYY-MM-DD");
        public long Status { get; set; }
        public int Position { get; set; }
        public long QuestionType { get; set; }

        public long? ParentId { get; set; }
        public virtual ICollection<Question> Question { get; set; }
        public virtual ICollection<MultiChoice> ChildMultiChoice { get; set; }
        public virtual Menu GetMenu { get; set; }
        public virtual MultiChoice GetMultiChoice { get; set; }
    }
}
