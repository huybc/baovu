﻿using System;
using System.Collections.Generic;

namespace KidInteractiveAPI.Models
{
    public partial class Question
    {
        public Question()
        {
            Answer = new HashSet<Answer>();
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public long Status { get; set; }
        public long Qtype { get; set; }
        public long Point { get; set; }
        public long QuestionGroupId { get; set; }
        public long? ParentId { get; set; }

        public virtual MultiChoice QuestionGroup { get; set; }
        public virtual ICollection<Answer> Answer { get; set; }
        public virtual ICollection<Question> ChildQuestion { get; set; }
        public virtual Question GetQuestion { get; set; }
    }
}
