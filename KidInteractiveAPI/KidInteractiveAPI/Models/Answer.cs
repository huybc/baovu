﻿using System;
using System.Collections.Generic;

namespace KidInteractiveAPI.Models
{
    public partial class Answer
    {
        public long Id { get; set; }
        public string Content { get; set; }
        public string Sound { get; set; }
        public string Image { get; set; }
        public long IsCorrect { get; set; }
        public long Status { get; set; }
        public long QuestionId { get; set; }

        public virtual Question Question { get; set; }
    }
}
