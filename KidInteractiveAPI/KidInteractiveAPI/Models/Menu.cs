﻿using System;
using System.Collections.Generic;

namespace KidInteractiveAPI.Models
{
    public partial class Menu
    {
        public Menu()
        {
            InverseParent = new HashSet<Menu>();
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public long Status { get; set; }
        public string Link { get; set; }
        public long LinkType { get; set; }
        public long? ParentId { get; set; }

        public virtual Menu Parent { get; set; }
        public virtual ICollection<MultiChoice> MultiChoices { get; set; }
        public virtual ICollection<Menu> InverseParent { get; set; }
    }
}
