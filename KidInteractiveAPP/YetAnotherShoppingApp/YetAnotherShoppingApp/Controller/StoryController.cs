﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherShoppingApp.Controller
{
    public class StoryController
    {
        private static string _api = "http://206.189.82.108:9119/";
        private static List<Menu> GetData(string url_webapi)
        {
            List<Menu> entries = new List<Menu>();
            try
            {
                HttpClient client = new HttpClient();
                var resp = client.GetAsync(url_webapi + "Menu/GetData").Result;
                string result = resp.Content.ReadAsStringAsync().Result;
                if (!string.IsNullOrEmpty(result))
                    entries = JsonConvert.DeserializeObject<List<Menu>>(result);
            }
            catch (Exception ex) { return entries = new List<Menu>(); }
            return entries;
        }
        public static List<Menu> GetParentsMenu() {
            var all = GetData(_api);
            var parents = all.Where(r => r.ParentId <= 0 || r.ParentId == null).ToList();
            return parents;
        }
        public static List<Menu> GetChilderns(int id)
        {
            var all = GetData(_api);
            return all.Where(r => r.ParentId != null && r.ParentId == id).ToList();
        }
        public static List<Menu> GetStoryCategories()
        {
            //Get menu parents
            var all = GetData(_api);
            var story_parent = all.Where(r => r.LinkType == 3).FirstOrDefault();
            if(story_parent != null)
            {
                var list_cate = all.Where(r => r.LinkType == 3 && r.ParentId == story_parent.Id).ToList();
                return list_cate;
            }
            return null;
        }
        public static List<Menu> GetListStories(int id)
        {
            if(id > 0)
            {
                var result = GetData(_api).Where(r => r.ParentId == id).ToList();
                return result;
            }
            return null;
        }
        public static Menu GetMaxLeaf(int id)
        {
            var all = GetData(_api);
            var affected = all.Where(r => r.Id == id).FirstOrDefault();
            if(affected != null)
            {
                //Kiem tra
                var ck = all.Where(r => r.ParentId == affected.Id).FirstOrDefault();
                if(ck == null)
                {
                    return affected;
                }
            }
            return null;
        }
        public static int CountChilds(int id)
        {
            var all = GetData(_api);
            return all.Where(r => r.ParentId == id).Count();
        }
    }
}
