﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.Controller;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Story
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class StoryCategories : Page
    {
        public List<YetAnotherShoppingApp.Menu> Cate;
        public StoryCategories()
        {
            this.InitializeComponent();
            Cate = StoryController.GetStoryCategories();
            var affected = Cate.FirstOrDefault();
            if (affected != null)
            {
                _title_cate.Text = affected.Title;
                _title_des.Text = affected.Description;
                _lst_story.Tag = affected.Id;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var id = 0;
            var a = int.TryParse(((Button)sender).Tag.ToString(), out id);
            if(id > 0)
            {
                var affected = Cate.Where(r => r.Id == id).FirstOrDefault();
                if(affected != null)
                {
                    _title_cate.Text = affected.Title;
                    _title_des.Text = affected.Description;
                    _lst_story.Tag = affected.Id;
                }
            }
        }

        private void _lst_story_Click(object sender, RoutedEventArgs e)
        {
            var tar_id = int.Parse(((Button)sender).Tag.ToString());
            this.Frame.Navigate(typeof(StoriesList), tar_id);
        }
    }
}
