﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.MainPage.Gamesss;
using YetAnotherShoppingApp;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Games
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectGames : Page
    {
        private List<YetAnotherShoppingApp.Menu> Games;
        public SelectGames()
        {
            this.InitializeComponent();
            Games = GamesController.GetGames();
        }

        private void btn_select_game_Click(object sender, RoutedEventArgs e)
        {
             
            var name = ((Button)sender).Tag.ToString();
            var aff = Games.Where(r => r.Title.Equals(name)).FirstOrDefault();
            if(aff != null)
                this.Frame.Navigate(typeof(PlayGame), aff);

        }
    }
}
