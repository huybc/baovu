﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.MainPage.Games;
using YetAnotherShoppingApp.Models;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Gamesss
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlayGame : Page
    {
        public PlayGame()
        {
            this.InitializeComponent();
        }
        //game_name
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var aff = (YetAnotherShoppingApp.Menu)e.Parameter;
            if(aff != null)
            {
                game_name.Text = aff.Title;
                var site = aff.Link;
                webview_main.Navigate(new Uri(site, UriKind.Absolute));
            }
            

        }

        private void btn_prev_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(SelectGames), null);
        }
    }
}
