﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.MainPage.Menu;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Quiz
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FinishQuestion : Page
    {
        public FinishQuestion()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            var details = (FinalGame)e.Parameter;
            if(details != null)
            {
                conguration.Text = string.Format("Chúc mừng bạn đã hoàn thành gói câu hỏi {0}", details.Topic);
                count.Text = string.Format("Trả lời đúng {0} / {1} cau hoi", details.TotalTrue, details.TotalQuest);
            }
            

        }

        //private void back_list_Click(object sender, RoutedEventArgs e)
        //{
        //    this.Frame.Navigate(typeof(Menu), null);
        //}

        private void back_home_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MenuCap1), null);
        }
    }
}
