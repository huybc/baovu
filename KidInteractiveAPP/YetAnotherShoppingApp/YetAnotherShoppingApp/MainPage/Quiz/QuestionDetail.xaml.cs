﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.ServiceModel.Channels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Composition;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.Models;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Quiz
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class QuestionDetail : Page
    {
        private MultiChoiceDetails details;
        private List<Answer> Answers;
        private List<ChoosenLog> Logs;
        private int _index = 1;
        public QuestionDetail()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);
            Logs = new List<ChoosenLog>();
            details = (MultiChoiceDetails)e.Parameter;
            foreach (var item in details.QuestionWithAnswers)
            {

                Logs.Add(new ChoosenLog { QuestionId = (int)item.Question.Id, Position = 0 });
            }
            Display();

        }


        private void btn_next_Click(object sender, RoutedEventArgs e)
        {
            _index = _index + 1;
            if (_index <= details.QuestionWithAnswers.Count())
            {
                
                RefreshPage();
                Display();

            }
            if (_index > details.QuestionWithAnswers.Count())
            {
                FinishQuestion();
            }
        }

        private void btn_prev_Click(object sender, RoutedEventArgs e)
        {
            if (_index > 1)
            {
                _index = _index - 1;
                RefreshPage();
                Display();

            }

        }

        private void ans_p1_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            OnlyChecked(cbx);
            var _checked = int.Parse(((CheckBox)sender).Tag.ToString());
            var isTrue = Validate(validate_p1, _checked);
            WriteLog(1, isTrue);
        }

        private void ans_p2_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            OnlyChecked(cbx);
            var _checked = int.Parse(((CheckBox)sender).Tag.ToString());
            var isTrue = Validate(validate_p2, _checked);
            WriteLog(2, isTrue);
        }

        private void ans_p3_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            OnlyChecked(cbx);
            var _checked = int.Parse(((CheckBox)sender).Tag.ToString());
            var isTrue = Validate(validate_p3, _checked);
            WriteLog(3, isTrue);
        }

        private void ans_p4_Checked(object sender, RoutedEventArgs e)
        {
            var cbx = (CheckBox)sender;
            OnlyChecked(cbx);
            var _checked = int.Parse(((CheckBox)sender).Tag.ToString());
            var isTrue = Validate(validate_p4, _checked);
            WriteLog(4, isTrue);
        }

        //private void Reset()
        //{
        //    ls_answer.SelectedItems.Clear();
        //}
        private void FinishQuestion()
        {
            var final = new FinalGame();
            final.Topic = details.Topic;
            final.TotalQuest = details.QuestionWithAnswers.Count();
            final.TotalTrue = Logs.Where(r => r.IsCorrect == true).Count();
            this.Frame.Navigate(typeof(FinishQuestion), final);
        }
        private void OnlyChecked(CheckBox check)
        {
            CheckBox[] checkboxes = new CheckBox[] { ans_p1, ans_p2,
                                             ans_p3, ans_p4 };
            checkboxes = checkboxes.Where(r => !r.Equals(check)).ToArray();
            foreach (var item in checkboxes)
                item.IsChecked = false;
        }
        private void Display()
        {
            var ans = details.QuestionWithAnswers.Skip(_index - 1).Take(1).FirstOrDefault();
            if (ans != null)
            {
                var r = ans.Answers;
                //ls_answer.ItemsSource = Answers;
                current_question.Text = "Câu số " + _index.ToString();
                main_question.Text = ans.Question.Content;
                main_question.TextWrapping = TextWrapping.Wrap;



                if (r.Count >= 1)
                {
                    ans_p1.Visibility = Visibility.Visible;
                    cont_p1.Visibility = Visibility.Visible;
                    validate_p1.Visibility = Visibility.Collapsed;
                    cont_p1.Text = r[0].Content;
                    ans_p1.Tag = r[0].IsCorrect;
                }

                if (r.Count >= 2)
                {
                    ans_p2.Visibility = Visibility.Visible;
                    cont_p2.Visibility = Visibility.Visible;
                    validate_p2.Visibility = Visibility.Collapsed;
                    cont_p2.Text = r[1].Content;
                    ans_p2.Tag = r[1].IsCorrect;
                }

                if (r.Count >= 3)
                {
                    ans_p3.Visibility = Visibility.Visible;
                    cont_p3.Visibility = Visibility.Visible;
                    validate_p3.Visibility = Visibility.Collapsed;
                    cont_p3.Text = r[2].Content;
                    ans_p3.Tag = r[2].IsCorrect;
                }

                if (r.Count >= 4)
                {
                    ans_p4.Visibility = Visibility.Visible;
                    cont_p4.Visibility = Visibility.Visible;
                    validate_p4.Visibility = Visibility.Collapsed;
                    cont_p4.Text = r[3].Content;
                    ans_p4.Tag = r[3].IsCorrect;
                }
                CheckLog();
            }
        }
        private void RefreshPage()
        {
            TextBlock[] textBlocks = new TextBlock[] { validate_p1, validate_p2, validate_p3, validate_p4 };
            foreach (var item in textBlocks)
            {
                item.Visibility = Visibility.Collapsed;
            }

            CheckBox[] checkboxes = new CheckBox[] { ans_p1, ans_p2,
                                             ans_p3, ans_p4 };
            foreach (var item in checkboxes)
            {
                item.IsChecked = false;
            }
            //Checklog
            var current_question = details.QuestionWithAnswers.Skip(_index - 1).Take(1).FirstOrDefault();
            var log = Logs.Where(r => r.QuestionId == current_question.Question.Id).FirstOrDefault();
            if (log != null && log.Position != 0)
            {
                var choosen = log.Position;
                switch (choosen)
                {
                    case 1:
                        ans_p1.IsChecked = true;
                        validate_p1.Visibility = Visibility.Visible;
                        break;
                    case 2:
                        ans_p2.IsChecked = true;
                        validate_p2.Visibility = Visibility.Visible;
                        break;
                    case 3:
                        ans_p3.IsChecked = true;
                        validate_p3.Visibility = Visibility.Visible;
                        break;
                    case 4:
                        ans_p4.IsChecked = true;
                        validate_p4.Visibility = Visibility.Visible;
                        break;
                }
            }



        }

        private void WriteLog(int position, bool isTrue)
        {
            var current_question = details.QuestionWithAnswers.Skip(_index - 1).Take(1).FirstOrDefault();
            var log = Logs.Where(r => r.QuestionId == current_question.Question.Id).FirstOrDefault();
            if (log != null)
            {
                log.Position = position;
                log.IsCorrect = isTrue;
            }
        }
        private void CheckLog()
        {
            var current_question = details.QuestionWithAnswers.Skip(_index - 1).Take(1).FirstOrDefault();
            var log = Logs.Where(r => r.QuestionId == current_question.Question.Id).FirstOrDefault();
            if (log != null)
            {
                switch (log.Position)
                {
                    case 1:
                        ans_p1.IsChecked = true;
                        break;
                    case 2:
                        ans_p2.IsChecked = true;
                        break;
                    case 3:
                        ans_p3.IsChecked = true;
                        break;
                    case 4:
                        ans_p4.IsChecked = true;
                        break;
                }
            }

        }

        private bool Validate(TextBlock block, int _checked)
        {
            if (_checked > 0)
            {
                block.Visibility = Visibility.Visible;
                block.Text = "Chính xác";
                return true;

            }
            else
            {
                block.Visibility = Visibility.Visible;
                block.Text = "Sai rồi";
                return false;
            }
        }


    }


    public class ChoosenLog
    {
        public int QuestionId { get; set; }
        public int Position { get; set; }
        public bool IsCorrect { get; set; } = false;
    }
    public class FinalGame
    {
        public string Topic { get; set; }
        public int TotalQuest { get; set; }
        public int TotalTrue { get; set; }
    }
}
