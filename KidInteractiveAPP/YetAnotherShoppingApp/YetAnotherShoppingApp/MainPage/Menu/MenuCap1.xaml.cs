﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.Controller;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Menu
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    
    public sealed partial class MenuCap1 : Page
    {
        private List<YetAnotherShoppingApp.Menu> MenuCon;
        private YetAnotherShoppingApp.Menu MenuCha;

        public MenuCap1()
        {
            
            this.InitializeComponent();
            var all = StoryController.GetParentsMenu();
            if (all != null)
            {
                MenuCha = all.FirstOrDefault();
                MenuCon = all.Skip(1).ToList();
                if (MenuCha != null)
                {
                    txt_title_left.Text = MenuCha.Title;
                    btn_left.Tag = ((int)MenuCha.Id).ToString();

                }
            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        private void btn_left_Click(object sender, RoutedEventArgs e)
        {
            var id = int.Parse(((Button)sender).Tag.ToString());
            if(id > 0)
            {
                this.Frame.Navigate(typeof(MenuCap2_3x3), id);
            }
        }

        private void btn_left_Click_1(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag.ToString();
            var id = int.Parse(tag);
            if (id > 0)
            {
                this.Frame.Navigate(typeof(MenuCap2_3x3), id);
            }
        }

        private void btn_item_menu_Click(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag.ToString();
            var id = int.Parse(tag);
            if (id > 0)
            {
                this.Frame.Navigate(typeof(MenuCap2_3x3), id);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DemoMultiChoice));
        }
    }
}
