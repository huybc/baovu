﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Printing3D;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.Controller;
using YetAnotherShoppingApp.Helper;
using YetAnotherShoppingApp.MainPage.EndView;
using YetAnotherShoppingApp.MainPage.Quiz;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Menu
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MenuCap2_3x3 : Page
    {
        public static string _api = "http://206.189.82.108:9119/";
        private List<YetAnotherShoppingApp.Menu> Menu2List;
        private List<MenuResize> Resized = new List<MenuResize>();
        private List<MenuResize> ResizedAfter = new List<MenuResize>();
        public double WidthDesign;
        public double HeightDesign;
        public int CountChild;
        public MenuCap2_3x3()
        {
            
            //Menu2List = DataRenderingController.RenderQuantityMenu(9);
            this.InitializeComponent();
            this.Loaded += ChallengePage_Loaded;
            //DataContext = this;

            //var b = ResizeButton(9);

            //WidthDesign = b.width;
            //HeightDesign = b.height;

        }


        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var id = (int)e.Parameter;
            if (id > 0)
            {
                Menu2List = StoryController.GetChilderns(id);
                CountChild = StoryController.CountChilds(id);
                foreach(var item in Menu2List)
                {
                    var a = new YetAnotherShoppingApp.MenuResize();
                    a.Height = HeightDesign;
                    a.Width = WidthDesign;
                    a.Menu = new YetAnotherShoppingApp.Menu();
                    a.Menu = item;
                    Resized.Add(a);
                }
                this.Loaded += ChallengePage_Loaded;
                //var b = ResizeButton(9);
                //WidthDesign = b.width;
                //HeightDesign = b.height;
            }
            

        }

        private void gridview_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void btn_item_menu_Click(object sender, RoutedEventArgs e)
        {
            var tag = ((Button)sender).Tag.ToString();
            var id = int.Parse(tag);
            if (id > 0)
            {
                var check = StoryController.GetMaxLeaf(id);
                if(check != null)
                {
                    if ((check.Description != null || check.Link != null) && check.LinkType != (int)LinkTypeEnum.Multi)
                    {
                        this.Frame.Navigate(typeof(WebView1), check);

                    }
                    if(check.LinkType == (int)LinkTypeEnum.Multi)
                    {
                        var details = new MultiChoiceDetails();
                        var multi_aff = MultiChoiceController.GetMultis(_api).Where(r => r.Category.Equals(check.Id.ToString())).FirstOrDefault();
                        if(multi_aff != null)
                        {
                            details.Topic = multi_aff.Topic;
                            details.Category = multi_aff.Category;
                            details.QuestionWithAnswers = new List<QuestionWithAnswers>();
                            var list_quest = MultiChoiceController.GetListQuestionByMultiChoiceId(_api, (int)multi_aff.Id);
                            foreach (var item in list_quest)
                            {
                                List<Answer> ls = new List<Answer>();
                                ls = MultiChoiceController.GetListAnswerByQuestionId(_api, (int)item.Id);
                                QuestionWithAnswers qwa = new QuestionWithAnswers(item, ls);
                                //Xem lai cho nay dhs loi
                                details.QuestionWithAnswers.Add(qwa);
                            }
                        }
                        this.Frame.Navigate(typeof(QuestionDetail), details);
                    }
                    
                }
                if (check == null)
                {
                    this.Frame.Navigate(typeof(MenuCap2_3x3), id);
                }
            }
            //kiem tra, neu khong con menu con nao thi show webview

        }

        private ButtonSize ResizeButton(int size, double width, double height)
        {
            double result_width = 0;
            double result_heigt = 0;
            if(size > 0)
            {
                if (size <= 4)
                {
                    result_width = width / 2 - 5;
                    result_heigt = height / 2 - 5;
                }
                if (size < 10 && size > 4)
                {
                    result_width = width / 3 - 5;
                    result_heigt = height / 3 - 5;

                }
                if (size > 10)
                {
                    result_width = width / 6 - 15;
                    result_heigt = width / 3 - 5;
                }
            }
            
            var result = new ButtonSize();
            result.width = result_width;
            result.height = result_heigt;
            return result;
        }
        void ChallengePage_Loaded(object sender, RoutedEventArgs e)
        {
            double width;
            double heigt;
            width = gridView.ActualWidth;
            heigt = gridView.ActualHeight;
            var b = ResizeButton(CountChild, width, heigt);
            foreach(var item in Resized)
            {
                item.Width = b.width;
                item.Height = b.height;
            }
            gridView.ItemsSource = Resized;
        }
    }
    public class ButtonSize
    {
        public double width { get; set; }
        public double height { get; set; }
    }

    
}
