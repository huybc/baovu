﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.MainPage.EndView;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.Menu
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DemoMultiChoice : Page
    {
        public DemoMultiChoice()
        {
            this.InitializeComponent();
        }

        private void doan_chu_Click(object sender, RoutedEventArgs e)
        {
            var t = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(WebViewDemo), t);
        }

        private void phim_dan_Click(object sender, RoutedEventArgs e)
        {
            var t = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(WebViewDemo), t);
        }

        private void keo_tha_Click(object sender, RoutedEventArgs e)
        {
            var t = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(WebViewDemo), t);
        }

        private void sap_xep_Click(object sender, RoutedEventArgs e)
        {
            var t = ((Button)sender).Tag.ToString();
            this.Frame.Navigate(typeof(WebViewDemo), t);
        }
    }
}
