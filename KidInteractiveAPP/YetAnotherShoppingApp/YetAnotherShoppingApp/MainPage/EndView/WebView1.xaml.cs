﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using YetAnotherShoppingApp.Helper;
using YetAnotherShoppingApp.MainPage.Story;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.EndView
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class WebView1 : Page
    {
        public string HtmlContent;
        public string domain = "http://kidinter.duckdns.org/";
        //public string domain = "http://localhost:5020/";
        public WebView1()
        {
            this.InitializeComponent();
            //webview_main.NavigateToString(HtmlContent);


        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var aff = (YetAnotherShoppingApp.Menu)e.Parameter;
            if (aff != null)
            {
                var a = (int)aff.Id;
                var type = aff.LinkType;
                if(type == (int)LinkTypeEnum.Story)
                    RunDetailStory(a);
                if (type == (int)LinkTypeEnum.Photo)
                    RunDetailPhoto(a);
                if (type == (int)LinkTypeEnum.Video)
                    RunDetailVideo(a);
                if (type == (int)LinkTypeEnum.Game)
                    RunDetailGame(aff.Link);

            }
        }
        private void RunDetailGame(string link)
        {
            //string domain = "localhost:5020/";
            //var site = domain + "Home/GameView?id=" + a + "";
            
            webview_main.Navigate(new Uri(link, UriKind.Absolute));
        }
        private void RunDetailStory(int a)
        {
            //string domain = "localhost:5020/";
            var site = domain + "Home/StoryView?id=" + a+"";
            webview_main.Navigate(new Uri(site, UriKind.Absolute));
        }
        private void RunDetailPhoto(int a)
        {
            //string domain = "localhost:5020/";
            var site = domain + "Home/PictureView?id=" + a + "";
            webview_main.Navigate(new Uri(site, UriKind.Absolute));
        }
        private void RunDetailVideo(int a)
        {
            //string domain = "localhost:5020/";
            var site = domain + "Home/VideoView?id=" + a + "";
            webview_main.Navigate(new Uri(site, UriKind.Absolute));
        }
    }
    public class StoryViewModel
    {
        public string body { get; set; }
    }
}
