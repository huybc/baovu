﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace YetAnotherShoppingApp.MainPage.EndView
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class WebViewDemo : Page
    {
        private string domain = "http://localhost:5020";
        public WebViewDemo()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var aff = (string)e.Parameter;
            if (!string.IsNullOrEmpty(aff))
            {
                switch (aff)
                {
                    case "doan_chu":
                        var site = domain + "/Home/FunKeyboard";
                        webview_main.Navigate(new Uri(site));
                        break;
                    case "phim_dan":
                        var site1 = domain + "/Home/FunPiano";
                        webview_main.Navigate(new Uri(site1));
                        break;
                }

            }
        }
    }
}
