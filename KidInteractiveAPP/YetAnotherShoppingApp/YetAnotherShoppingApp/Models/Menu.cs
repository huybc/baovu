﻿using System;
using System.Collections.Generic;

namespace YetAnotherShoppingApp
{
    public partial class Menu
    {
        public Menu()
        {
            InverseParent = new HashSet<Menu>();
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public long Status { get; set; }
        public string Link { get; set; }
        public long LinkType { get; set; }
        public long? ParentId { get; set; }

        public virtual Menu Parent { get; set; }
        public virtual ICollection<Menu> InverseParent { get; set; }
        
    }
    public class MenuResize
    {
        public YetAnotherShoppingApp.Menu Menu { get; set; }
        public double Width { get; set; } = 0;
        public double Height { get; set; } = 0;
    }
}
