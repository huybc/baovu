﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherShoppingApp { 
    public class GameInfo
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
