﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YetAnotherShoppingApp.Helper
{
    public enum LinkTypeEnum
    {
        /*
         <select class="form-control" id="LinkType">

                        <option value="1">Photo</option>
                        <option value="2">Video</option>
                        <option value="3">Story</option>
                        <option value="4">Game</option>
                        <option value="6">Multi Choice</option>
                        <option value="7">Menu</option>
                    </select>
         */
        Photo = 1,
        Video = 2,
        Story = 3,
        Game = 4,
        Multi = 6,
        Menu = 7
    }
}
