﻿using System;
using System.Collections.Generic;

namespace DataAccessLibrary.Models
{
    public partial class Question
    {
        public Question()
        {
            Answer = new HashSet<Answer>();
        }

        public long Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public long Status { get; set; }
        public long Qtype { get; set; }
        public long Point { get; set; }
        public long QuestionGroupId { get; set; }

        public virtual MultiChoice QuestionGroup { get; set; }
        public virtual ICollection<Answer> Answer { get; set; }
    }
}
