﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.Models
{
    class TreeNode 
    {
        public Menu GetMenu { get; set; }
        public List<Menu> Children { get; private set; }
        
    }
}
