﻿using DataAccessLibrary.Models;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.DataAccess_Offline
{
    public class Copyright_Off
    {
        public static List<Copyright> GetData(string dbpath)
        {
            List<Copyright> entries = new List<Copyright>();
            Copyright cpr = null;
            try
            {
                using (SqliteConnection db =
                   new SqliteConnection($"Filename={dbpath}"))
                {
                    db.Open();
                    SqliteCommand selectCommand = new SqliteCommand
                        ("Select * from Copyright", db);
                    SqliteDataReader query = selectCommand.ExecuteReader();
                    while (query.Read())
                    {
                        cpr = new Copyright()
                        {
                            CompanyName = query.GetString(query.GetOrdinal("CompanyName")),
                            YearOfCopy = query.GetInt64(query.GetOrdinal("YearOfCopy")),
                            LicenseType = query.GetString(query.GetOrdinal("LicenseType")),
                            Address = query.GetString(query.GetOrdinal("Address")),
                            Telephone = query.GetString(query.GetOrdinal("Telephone")),
                            Email = query.GetString(query.GetOrdinal("Email")),
                        };
                        entries.Add(cpr);
                    }
                    db.Close();
                }
            }
            catch(Exception ex) { return entries = new List<Copyright>(); }
            return entries;
        }
    }
}
