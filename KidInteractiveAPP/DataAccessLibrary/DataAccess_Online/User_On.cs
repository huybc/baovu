﻿using DataAccessLibrary.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace DataAccessLibrary.DataAccess_Offline
{
    public class User_On
    {
        public static string Login(string url_webapi, string username, string password)
        {
            string message = Const.DangNhapThanhCong;
            try
            {
                HttpClient client = new HttpClient();
                InputModel obj = new InputModel();
                obj.Email = username;
                obj.Password = password;
                obj.RememberMe = false;
                string jsonObj = JsonConvert.SerializeObject(obj);
                var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                var resp = client.PostAsync(url_webapi + "User/Login", content).Result;
                string result = resp.Content.ReadAsStringAsync().Result;
                if(result == "false")
                {
                    message = Const.DangNhapThatBai;
                }    
            }
            catch (Exception ex) { return ex.Message; }
            return message;
        }
    }
}
