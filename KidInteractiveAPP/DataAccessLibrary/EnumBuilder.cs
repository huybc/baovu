﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary
{
    public enum MenuType
    {
        Story = 3,
        Games = 4,
        Video = 1,
        Images = 2,
        MultiChoice = 5
    }
}
