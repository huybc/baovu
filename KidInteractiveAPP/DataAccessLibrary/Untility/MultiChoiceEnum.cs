﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccessLibrary.Untility
{
    public enum MultiChoiceEnumStatus
    {
        Publish = 1,
        Archive = 2
    }
    public enum MultiChoiceQuestionType
    {
        Funny = 1,
        Scary = 2
    }
    public class MultiChoiceCombo
    {
        public int Number { get; set; }
        public string Name { get; set; }
    }
}
