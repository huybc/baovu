﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using muxc = Microsoft.UI.Xaml.Controls;
using DataAccessLibrary;
using KidInteractiveAPP.Utilities;
using Windows.Storage;
using Windows.Storage.Pickers;
using Microsoft.UI.Xaml.Media.Imaging;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP.Menu
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MenuPage : Page
    {
        private string menuId = "";
        private ObservableCollection<Item> DataSource = new ObservableCollection<Item>();
        private ObservableCollection<objCombo> StatusList = new ObservableCollection<objCombo>();
        private ObservableCollection<objCombo> TypeList = new ObservableCollection<objCombo>();
        private ObservableCollection<objCombo> ParentList = new ObservableCollection<objCombo>();
        
        public MenuPage()
        {
            menuId = "";            
            this.InitializeComponent();
            titleHeader.Text = "Thêm mới danh mục";
            btn_add.Visibility = Visibility.Visible;
            btn_update.Visibility = Visibility.Collapsed;
            btn_delete.Visibility = Visibility.Collapsed;
            btn_removeupload.Visibility = Visibility.Collapsed;
            Icon.Visibility = Visibility.Collapsed;
            List<DataAccessLibrary.Models.Menu> dsTree = DataAccess.Menu_GetData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi());
            DataSource = GetDessertData(dsTree);            
            //
            StatusList.Add(new objCombo()
            {
                ID = 1,
                Name = "Publish"
            });
            StatusList.Add(new objCombo()
            {
                ID = 2,
                Name = "Archive"
            });
            TypeList.Add(new objCombo()
            {
                ID = 1,
                Name = "Photo"
            });
            TypeList.Add(new objCombo()
            {
                ID = 2,
                Name = "Video"
            });
            TypeList.Add(new objCombo()
            {
                ID = 3,
                Name = "Story"
            });
            TypeList.Add(new objCombo()
            {
                ID = 4,
                Name = "Game"
            });
            ParentList.Add(new objCombo()
            {
                ID = -1,
                Name = "---Selected---"
            });
            foreach (var obj in dsTree)
            {
                ParentList.Add(new objCombo()
                {
                    ID = Convert.ToInt32(obj.Id),
                    Name = obj.Title
                });
            }
            //            
            
        }

        private ObservableCollection<Item> GetDessertData(List<DataAccessLibrary.Models.Menu> dsTree)
        {
            var list = new ObservableCollection<Item>();                       
            ObservableCollection<Item> dsTreeTemp = new ObservableCollection<Item>();
            foreach(var o in dsTree)
            {
                Item tmp = new Item();
                tmp.Id = o.Id.ToString();
                tmp.Title = o.Title;
                tmp.ParentId = o.ParentId.ToString();
                tmp.InverseParent = new ObservableCollection<Item>();
                dsTreeTemp.Add(tmp);
            }    
            Dictionary<string, int> map = new Dictionary<string, int>();
            for (int i = 0; i < dsTreeTemp.Count(); i++)
            {                
                map.Add(dsTreeTemp[i].Id.ToString(), i);
            }
            for (int i = 0; i < dsTreeTemp.Count(); i++)
            {
                if (string.IsNullOrEmpty(dsTreeTemp[i].ParentId))
                {
                    //dsTree[i].InverseParent = new ObservableCollection<DataAccessLibrary.Models.Menu>();
                    list.Add(dsTreeTemp[i]);
                }
                else
                {
                    Int32 idx = map[Convert.ToInt32(dsTreeTemp[i].ParentId).ToString()];
                    dsTreeTemp[idx].InverseParent.Add(dsTreeTemp[i]);
                }
            }

            //list.Add(flavorsCategory);

            return list;
        }                

        private void DessertTree_ItemInvoked(TreeView sender, TreeViewItemInvokedEventArgs args)
        {
            var objItem = (Item)args.InvokedItem;
            menuId = objItem.Id;
            titleHeader.Text = "Chi tiết danh mục";
            btn_add.Visibility = Visibility.Collapsed;
            btn_update.Visibility = Visibility.Visible;
            btn_delete.Visibility = Visibility.Visible;
            var objMenu = DataAccess.Menu_objGetData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), objItem.Id);            
            if(!string.IsNullOrEmpty(objMenu.Title))
                title.Text = objMenu.Title;
            if (!string.IsNullOrEmpty(objMenu.Description))
                description.Text = objMenu.Description;
            if (!string.IsNullOrEmpty(objMenu.Icon))
            {
                try
                {
                    Icon.Text = objMenu.Icon;
                    var imageBytes = new BitmapImage(new Uri(objMenu.Icon));
                    imageView.Source = imageBytes;
                }
                catch { }
            }
            else
            {
                btn_removeupload.Visibility = Visibility.Collapsed;                
            }
            if (!string.IsNullOrEmpty(objMenu.Link))
                Link.Text = objMenu.Link;
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
            if (objMenu.Status != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
                cmbStatus.SelectedItem = StatusList.SingleOrDefault(o => o.ID == objMenu.Status);
#pragma warning disable CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
            if (objMenu.LinkType != null)
#pragma warning restore CS0472 // The result of the expression is always 'true' since a value of type 'long' is never equal to 'null' of type 'long?'
                cmbLinkType.SelectedItem = TypeList.SingleOrDefault(o => o.ID == objMenu.LinkType);            
            if (objMenu.ParentId != null)
                cmbParent.SelectedItem = ParentList.SingleOrDefault(o => o.ID == objMenu.ParentId);
        }
        //private async void OpenButton_Click(object sender, RoutedEventArgs e)
        //{
        //    // Open a text file.
        //    Windows.Storage.Pickers.FileOpenPicker open =
        //        new Windows.Storage.Pickers.FileOpenPicker();
        //    open.SuggestedStartLocation =
        //        Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
        //    open.FileTypeFilter.Add(".rtf");

        //    Windows.Storage.StorageFile file = await open.PickSingleFileAsync();

        //    if (file != null)
        //    {
        //        try
        //        {
        //            Windows.Storage.Streams.IRandomAccessStream randAccStream =
        //        await file.OpenAsync(Windows.Storage.FileAccessMode.Read);

        //            // Load the file into the Document property of the RichEditBox.
        //            editor.Document.LoadFromStream(Windows.UI.Text.TextSetOptions.FormatRtf, randAccStream);
        //        }
        //        catch (Exception)
        //        {
        //            ContentDialog errorDialog = new ContentDialog()
        //            {
        //                Title = "File open error",
        //                Content = "Sorry, I couldn't open the file.",
        //                PrimaryButtonText = "Ok"
        //            };

        //            await errorDialog.ShowAsync();
        //        }
        //    }
        //}

        //private async void SaveButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Windows.Storage.Pickers.FileSavePicker savePicker = new Windows.Storage.Pickers.FileSavePicker();
        //    savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;

        //    // Dropdown of file types the user can save the file as
        //    savePicker.FileTypeChoices.Add("Rich Text", new List<string>() { ".rtf" });

        //    // Default file name if the user does not type one in or select a file to replace
        //    savePicker.SuggestedFileName = "New Document";

        //    Windows.Storage.StorageFile file = await savePicker.PickSaveFileAsync();
        //    if (file != null)
        //    {
        //        // Prevent updates to the remote version of the file until we
        //        // finish making changes and call CompleteUpdatesAsync.
        //        Windows.Storage.CachedFileManager.DeferUpdates(file);
        //        // write to file
        //        Windows.Storage.Streams.IRandomAccessStream randAccStream =
        //            await file.OpenAsync(Windows.Storage.FileAccessMode.ReadWrite);

        //        editor.Document.SaveToStream(Windows.UI.Text.TextGetOptions.FormatRtf, randAccStream);

        //        // Let Windows know that we're finished changing the file so the
        //        // other app can update the remote version of the file.
        //        Windows.Storage.Provider.FileUpdateStatus status = await Windows.Storage.CachedFileManager.CompleteUpdatesAsync(file);
        //        if (status != Windows.Storage.Provider.FileUpdateStatus.Complete)
        //        {
        //            Windows.UI.Popups.MessageDialog errorBox =
        //                new Windows.UI.Popups.MessageDialog("File " + file.Name + " couldn't be saved.");
        //            await errorBox.ShowAsync();
        //        }
        //    }
        //}

        //private void BoldButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Windows.UI.Text.ITextSelection selectedText = editor.Document.Selection;
        //    if (selectedText != null)
        //    {
        //        Windows.UI.Text.ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
        //        charFormatting.Bold = Windows.UI.Text.FormatEffect.Toggle;
        //        selectedText.CharacterFormat = charFormatting;
        //    }
        //}

        //private void ItalicButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Windows.UI.Text.ITextSelection selectedText = editor.Document.Selection;
        //    if (selectedText != null)
        //    {
        //        Windows.UI.Text.ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
        //        charFormatting.Italic = Windows.UI.Text.FormatEffect.Toggle;
        //        selectedText.CharacterFormat = charFormatting;
        //    }
        //}

        //private void UnderlineButton_Click(object sender, RoutedEventArgs e)
        //{
        //    Windows.UI.Text.ITextSelection selectedText = editor.Document.Selection;
        //    if (selectedText != null)
        //    {
        //        Windows.UI.Text.ITextCharacterFormat charFormatting = selectedText.CharacterFormat;
        //        if (charFormatting.Underline == Windows.UI.Text.UnderlineType.None)
        //        {
        //            charFormatting.Underline = Windows.UI.Text.UnderlineType.Single;
        //        }
        //        else
        //        {
        //            charFormatting.Underline = Windows.UI.Text.UnderlineType.None;
        //        }
        //        selectedText.CharacterFormat = charFormatting;
        //    }
        //}
        
        private async void btn_upload_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FileOpenPicker openPicker = new FileOpenPicker();
                openPicker.ViewMode = PickerViewMode.Thumbnail;
                openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
                openPicker.FileTypeFilter.Add(".jpg");
                openPicker.FileTypeFilter.Add(".png");

                StorageFile file = await openPicker.PickSingleFileAsync();
                string path = file.Path;
                if (file != null)
                {
                    var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read);
                    var imageBytes = new BitmapImage();
                    imageBytes.SetSource(stream);
                    imageView.Source = imageBytes;
                    imageView.Height = 200;
                    Icon.Text = ApplicationData.Current.LocalFolder.Path + "\\" + Path.GetFileName(path);
                    await file.CopyAsync(ApplicationData.Current.LocalFolder, Path.GetFileName(path), NameCollisionOption.ReplaceExisting);
                    btn_removeupload.Visibility = Visibility.Visible;
                }
            }
            catch { }
        }
        private void btn_removeupload_Click(object sender, RoutedEventArgs e)
        {
            imageView.Source = null;
            imageView.Height = 0;
            Icon.Text = "";
            btn_removeupload.Visibility = Visibility.Collapsed;
        }
        
        private void btn_refesh_Click(object sender, RoutedEventArgs e)
        {
            //
            titleHeader.Text = "Thêm mới danh mục";
            btn_add.Visibility = Visibility.Visible;
            btn_update.Visibility = Visibility.Collapsed;
            btn_delete.Visibility = Visibility.Collapsed;
            btn_removeupload.Visibility = Visibility.Collapsed;
            imageView.Source = null;
            menuId = "";
            title.Text = "";
            description.Text = "";
            Icon.Text = "";
            Link.Text = "";
            cmbStatus.SelectedValue = "";
            cmbLinkType.SelectedValue = "";
            cmbParent.SelectedValue = "";
        }

        private void btn_add_Click(object sender, RoutedEventArgs e)
        {
            DataAccessLibrary.Models.Menu oMenu = new DataAccessLibrary.Models.Menu();
            oMenu.Title = title.Text;
            oMenu.Description = description.Text;
            oMenu.Icon = Icon.Text;
            oMenu.Link = Link.Text;
            if (cmbStatus.SelectedItem != null)
                oMenu.Status = ((objCombo)cmbStatus.SelectedItem).ID;
            if (cmbLinkType.SelectedItem != null)
                oMenu.LinkType = ((objCombo)cmbLinkType.SelectedItem).ID;
            if (cmbParent.SelectedItem != null)
            {
                if (((objCombo)cmbParent.SelectedItem).ID == -1)
                    oMenu.ParentId = null;
                else
                    oMenu.ParentId = ((objCombo)cmbParent.SelectedItem).ID;
            }
            DataAccess.Menu_InsertData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), oMenu);
            //
            this.Frame.Navigate(typeof(MenuPage));
            this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
        }

        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            DataAccessLibrary.Models.Menu oMenu = new DataAccessLibrary.Models.Menu();
            oMenu.Id = Convert.ToInt64(menuId);
            oMenu.Title = title.Text;
            oMenu.Title = title.Text;
            oMenu.Description = description.Text;
            oMenu.Icon = Icon.Text;
            oMenu.Link = Link.Text;
            if (cmbStatus.SelectedItem != null)
                oMenu.Status = ((objCombo)cmbStatus.SelectedItem).ID;
            if (cmbLinkType.SelectedItem != null)
                oMenu.LinkType = ((objCombo)cmbLinkType.SelectedItem).ID;
            if (cmbParent.SelectedItem != null)
            {
                if(((objCombo)cmbParent.SelectedItem).ID == -1)
                    oMenu.ParentId = null;
                else                
                    oMenu.ParentId = ((objCombo)cmbParent.SelectedItem).ID;
            }
            DataAccess.Menu_UpdateData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), oMenu);
            this.Frame.Navigate(typeof(MenuPage));
            this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
        }

        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            DataAccess.Menu_DeleteData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), menuId);
            this.Frame.Navigate(typeof(MenuPage));
            this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
        }
        //

    }
    public class Item
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string ParentId { get; set; }
        public ObservableCollection<Item> InverseParent { get; set; } = new ObservableCollection<Item>();

        public override string ToString()
        {
            return Title;
        }        
    }
    public class objCombo
    {
        public int ID
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }
        public override string ToString()
        {
            return this.Name;
        }
    }
}
