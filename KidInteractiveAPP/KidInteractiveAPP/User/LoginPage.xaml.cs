﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Controls.Primitives;
using Microsoft.UI.Xaml.Data;
using Microsoft.UI.Xaml.Input;
using Microsoft.UI.Xaml.Media;
using Microsoft.UI.Xaml.Navigation;
using KidInteractiveAPP.User;
using DataAccessLibrary.DataAccess_Offline;
using KidInteractiveAPP.Utilities;
using DataAccessLibrary;
using KidInteractiveAPP.Menu;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
        public LoginPage()
        {
            this.InitializeComponent();
        }
        private void ContentFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }
        private void PassportSignInButton_Click(object sender, RoutedEventArgs e)
        {
            string message = DataAccess.User_Login( CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), UsernameTextBox.Text, UsernamePasswordBox.Password);
            if(message == Const.DangNhapThanhCong)
            {
                ErrorMessage.Text = message;
                Windows.Storage.ApplicationData.Current.LocalSettings.Values["isLoginSuccess"] = true;                
                this.Frame.Navigate(typeof(AppsPage));
                this.Frame.BackStack.RemoveAt(this.Frame.BackStack.Count - 1);
            }   
            else
            {
                ErrorMessage.Text = message;
            }                
        }
        private void SignOut_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(RegisterPage));
        }

        private void RegisterButtonTextBlock_OnPointerPressed(object sender, PointerRoutedEventArgs e)
        {
            ErrorMessage.Text = "";
        }
    }
}
