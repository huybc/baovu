﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.UI.Xaml;
using Microsoft.UI.Xaml.Controls;
using Microsoft.UI.Xaml.Navigation;
using DataAccessLibrary.Untility;
using System.Collections.ObjectModel;
using DataAccessLibrary;
using KidInteractiveAPP.Utilities;
using DataAccessLibrary.Models;
// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace KidInteractiveAPP.MultiChoice
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class QuestionManager : Page
    {
        private ObservableCollection<MultiChoiceCombo> status = new ObservableCollection<MultiChoiceCombo>();
        private ObservableCollection<MultiChoiceCombo> qType = new ObservableCollection<MultiChoiceCombo>();
        private ObservableCollection<Answer> Ans = new ObservableCollection<Answer>();
        private ObservableCollection<ConvertAnswer> C_Ans = new ObservableCollection<ConvertAnswer>();
        private ObservableCollection<Question> Ques = new ObservableCollection<Question>();
        //private List<Answer> Ans;
        public QuestionManager()
        {
            var sList = Enum.GetValues(typeof(MultiChoiceEnumStatus))
               .Cast<MultiChoiceEnumStatus>()
               .Select(t => new MultiChoiceCombo
               {
                   Number = ((int)t),
                   Name = t.ToString()
               });
            var qtList = Enum.GetValues(typeof(MultiChoiceQuestionType))
               .Cast<MultiChoiceQuestionType>()
               .Select(t => new MultiChoiceCombo
               {
                   Number = ((int)t),
                   Name = t.ToString()
               });
            foreach (var item in sList)
            {
                status.Add(item);
            }
            foreach (var item in qtList)
            {
                qType.Add(item);
            }
            this.InitializeComponent();
            titleHeader.Text = "Question Management";
            titleHeaderAns.Text = "Answers Management";
            //var result = DataAccess.Question_GetData(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi());
            //foreach (var item in result)
            //{
            //    Ques.Add(item);
            //}
            //G_Question.ItemsSource = Ques;

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            var id_string = (string)e.Parameter;
            //titleHeader.Text = "Modify Id - " + id_string;
            var id = 0;
            var a = int.TryParse(id_string, out id);
            if (id > 0)
            {
                var result = DataAccess.Question_GetDataByMultiChoiceId(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id);
                if (result != null)
                {
                    Ques = ConvertToObservableCollection(result);
                    G_Question.ItemsSource = Ques;
                }
                MultiChoiceId.Text = id.ToString();
            }
        }
        private void btn_create_question_Click(object sender, RoutedEventArgs e)
        {
            var sList = Enum.GetValues(typeof(MultiChoiceEnumStatus))
               .Cast<MultiChoiceEnumStatus>()
               .Select(t => new MultiChoiceCombo
               {
                   Number = ((int)t),
                   Name = t.ToString()
               });
            var qtList = Enum.GetValues(typeof(MultiChoiceQuestionType))
               .Cast<MultiChoiceQuestionType>()
               .Select(t => new MultiChoiceCombo
               {
                   Number = ((int)t),
                   Name = t.ToString()
               });
            var s = 0;
            var qt = 0;
            if (StatusCombo.SelectedItem != null)
            {
                var container = (MultiChoiceCombo)StatusCombo.SelectedItem;
                s = container.Number;
            }
            if (QType.SelectedItem != null)
            {
                var container = (MultiChoiceCombo)QType.SelectedItem;
                qt = container.Number;
            }
            var multi_id = string.IsNullOrEmpty(MultiChoiceId.Text) == true ? 0 : int.Parse(MultiChoiceId.Text);
            var newborn = new Question();
            newborn.Title = title.Text;
            newborn.Content = content.Text;
            var p = 0;
            var a = int.TryParse(point.Text, out p);
            newborn.Point = p;
            newborn.Qtype = qt;
            newborn.Status = s;
            newborn.QuestionGroupId = multi_id;
            if (titleHeader.Text.Equals("Question Management"))
            {
                newborn.Id = 0;
            }
            if (titleHeader.Text.StartsWith("Modify Id"))
            {
                newborn.Id = int.Parse(titleHeader.Text.Split("-")[1].Trim());
            }
            try
            {
                var worked = DataAccess.Question_SaveQuestion(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), newborn);
                foreach(var item in C_Ans)
                {
                    var aff = new Answer() { Id = item.Id, Content = item.Content, IsCorrect = item.IsCorrect == true ? 1 : 0, Status = item.Status, QuestionId = item.QuestionId };
                    var worked_ans = DataAccess.Answer_SaveAnswer(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), aff);
                }
                var result = DataAccess.Question_GetDataByMultiChoiceId(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), multi_id);
                Ques = ConvertToObservableCollection(result);
                G_Question.ItemsSource = Ques;
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex)
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
            {
                //do stuff
            }

        }

        private void affected_question_Click(object sender, RoutedEventArgs e)
        {
            var affected_id = int.Parse(((Button)sender).Tag.ToString());
            var res = DataAccess.Question_GetById(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), affected_id);
            if (res != null)
            {
                titleHeader.Text = "Modify Id - " + res.Id;
                title.Text = res.Title;
                content.Text = res.Content;
                point.Text = res.Point.ToString();
                StatusCombo.SelectedItem = status.SingleOrDefault(r => r.Number == res.Status);
                QType.SelectedItem = qType.SingleOrDefault(r => r.Number == res.Qtype);
                btn_remove_question.Tag = res.Id;
                /*
                 <TextBox x:Name="title" Header= "Title" Margin="0,24,0,0" MaxLength="320" Width="400" HorizontalAlignment="Left"/>
                        <TextBox x:Name="content" AcceptsReturn="True" TextWrapping="Wrap"
                         Height="333" Width="400" Header="Content"
                         ScrollViewer.VerticalScrollBarVisibility="Auto"/>
                        <TextBox x:Name="point" Header="Point"></TextBox>
                        <ComboBox x:Name="QType" Margin="0,24,0,0" Header="Question Type" ItemsSource="{x:Bind qType}" DisplayMemberPath="Name" SelectedValuePath="Number"></ComboBox>
                        <ComboBox x:Name="StatusCombo" Header="Status" Margin="0,24,0,0" ItemsSource="{x:Bind status}" DisplayMemberPath="Name" SelectedValuePath="Number"></ComboBox>
                 */

                //Lay du lieu cau tra loi

                var result = DataAccess.Answer_GetDataByQuestionId(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), (int)res.Id);
                //ObservableCollection<Answer> Ans = new ObservableCollection<Answer>();
                Ans = ConvertToObservableCollection(result);
                C_Ans = ConvertAnswersToObservableCollection(result);
                ans_list.ItemsSource = C_Ans;
            }

        }

        
        private void btn_add_answer_Click(object sender, RoutedEventArgs e)
        {
            var id = 0;
            if (titleHeader.Text.StartsWith("Modify Id"))
            {
                id = int.Parse(titleHeader.Text.Split("-")[1].Trim());
            }
            var res = DataAccess.Answer_GetDataByQuestionId(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id);
            var new_ans = new Answer() { QuestionId = id, Status = 1, Content = "", Id = 0, IsCorrect = 0 };
            res.Add(new_ans);
            Ans = ConvertToObservableCollection(res);
            C_Ans = ConvertAnswersToObservableCollection(res);
            
            ans_list.ItemsSource = C_Ans;
            
        }
        private void btn_new_question_Click(object sender, RoutedEventArgs e)
        {
            titleHeader.Text = "Question Management";
            title.Text = "";
            content.Text = "";
            point.Text = "";
            var nList = new List<Answer>();
            C_Ans = ConvertAnswersToObservableCollection(nList);
            ans_list.ItemsSource = C_Ans;
            //IAsyncAction asyncAction = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            //{
            //    C_Ans.Clear();
            //});
            
        }
        

        private void btn_remove_question_Click(object sender, RoutedEventArgs e)
        {
            var id = 0;
            var id_multi = int.Parse(MultiChoiceId.Text);
            if (titleHeader.Text.StartsWith("Modify Id"))
            {
                id = int.Parse(titleHeader.Text.Split("-")[1].Trim());
            }
            if(id > 0)
            {
                var re = DataAccess.Question_Delete(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id);
                var reload = DataAccess.Question_GetDataByMultiChoiceId(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id_multi);
                Ques = ConvertToObservableCollection(reload);
                G_Question.ItemsSource = Ques;
                titleHeader.Text = "Question Management";
                title.Text = "";
                content.Text = "";
                point.Text = "";
                var nList = new List<Answer>();
                C_Ans = ConvertAnswersToObservableCollection(nList);
                ans_list.ItemsSource = C_Ans;
            }

        }

        private void btn_remove_answer_Click(object sender, RoutedEventArgs e)
        {
            var id_string = int.Parse(((Button)sender).Tag.ToString());
            var id_quest = 0;
            if (titleHeader.Text.StartsWith("Modify Id"))
            {
                id_quest = int.Parse(titleHeader.Text.Split("-")[1].Trim());
            }
            if (id_string > 0)
            {
                //Delete
                var re = DataAccess.Answer_DeleteAnswer(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id_string);
                var reload = DataAccess.Answer_GetDataByQuestionId(CoreConfig.OnlineMode(), CoreConfig.DatabaseName(), CoreConfig.Url_WebApi(), id_quest);
                C_Ans = ConvertAnswersToObservableCollection(reload);
                ans_list.ItemsSource = C_Ans;
                //ContentDialog dialog = new ContentDialog
                //{
                //    Title = "Are you sure to remove answer?",
                //    PrimaryButtonText = "Ok",
                //    CloseButtonText = "Cancel"

                //};
                //ContentDialogResult result = await dialog.ShowAsync();
                //if(result == ContentDialogResult.Primary)
                //{

                //}
                //else
                //{
                //    //Do nothing
                //}
            }
        }

        private ObservableCollection<Answer> ConvertToObservableCollection(List<Answer> answers)
        {
            var result = new ObservableCollection<Answer>();
            foreach (var item in answers)
            {
                result.Add(item);
            }
            return result;
        }
        private ObservableCollection<Question> ConvertToObservableCollection(List<Question> questions)
        {
            var result = new ObservableCollection<Question>();
            foreach (var item in questions)
            {
                result.Add(item);
            }
            return result;
        }

        private ObservableCollection<ConvertAnswer> ConvertAnswersToObservableCollection(List<Answer> ans)
        {
            var result = new ObservableCollection<ConvertAnswer>();
            foreach (var item in ans)
            {
                result.Add(new ConvertAnswer() { Id = item.Id, Content = item.Content, Status = item.Status, IsCorrect = item.IsCorrect > 0 ? true : false, QuestionId = item.QuestionId });
            }
            return result;
        }

    }    
}
