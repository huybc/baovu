﻿#pragma checksum "E:\DuAn\master\KidInteractiveAPP\KidInteractiveAPP\GamesPage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "D2D4DFEAEA8EF00A0C52BF05E716581100FC82CC282755023C0E3E319E6CB09F"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KidInteractiveAPP
{
    partial class GamesPage : 
        global::Microsoft.UI.Xaml.Controls.Page, 
        global::Microsoft.UI.Xaml.Markup.IComponentConnector,
        global::Microsoft.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.UI.Xaml.Markup.Compiler"," 0.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 2: // GamesPage.xaml line 24
                {
                    global::Microsoft.UI.Xaml.Controls.Button element2 = (global::Microsoft.UI.Xaml.Controls.Button)(target);
                    ((global::Microsoft.UI.Xaml.Controls.Button)element2).Click += this.go_funnygame_click;
                }
                break;
            case 3: // GamesPage.xaml line 29
                {
                    global::Microsoft.UI.Xaml.Controls.Button element3 = (global::Microsoft.UI.Xaml.Controls.Button)(target);
                    ((global::Microsoft.UI.Xaml.Controls.Button)element3).Click += this.go_multichoice_click;
                }
                break;
            case 4: // GamesPage.xaml line 34
                {
                    global::Microsoft.UI.Xaml.Controls.Button element4 = (global::Microsoft.UI.Xaml.Controls.Button)(target);
                    ((global::Microsoft.UI.Xaml.Controls.Button)element4).Click += this.go_categoriesgame_click;
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        /// <summary>
        /// GetBindingConnector(int connectionId, object target)
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.UI.Xaml.Markup.Compiler"," 0.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Microsoft.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Microsoft.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

