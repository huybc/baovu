﻿using DataAccessLibrary;
using DataAccessLibrary.Models;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using Windows.ApplicationModel;
using Windows.Storage;

namespace KidInteractiveAPP.Utilities
{
    public class CoreConfig
    {
        public static void InitializeDatabase()
        {
            try
            {
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the 'await' operator to the result of the call.
                ApplicationData.Current.LocalFolder.CreateFileAsync("KidInteractive.db", CreationCollisionOption.OpenIfExists);
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed. Consider applying the 'await' operator to the result of the call.
                string dbpath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "KidInteractive.db");
                using (SqliteConnection db =
                   new SqliteConnection($"Filename={dbpath}"))
                {
                    db.Open();

                    String tableCommand = "CREATE TABLE IF NOT " +
                        "EXISTS Answer ( " +
                        " ID    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        " Content   TEXT NOT NULL, " +
                        " IsCorrect INTEGER NOT NULL DEFAULT 0, " +
                        " Status    INTEGER NOT NULL DEFAULT 1, " +
                        " QuestionID    INTEGER NOT NULL, " +
                        " CONSTRAINT FKQAnswer FOREIGN KEY(QuestionID) REFERENCES Question(ID) " +
                        "); ";

                    SqliteCommand createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                        "EXISTS AspNetRoleClaims ( " +
                         "   Id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                         "   RoleId    TEXT NOT NULL, " +
                         "  ClaimType TEXT, " +
                         "  ClaimValue    TEXT, " +
                         "  CONSTRAINT FK_AspNetRoleClaims_AspNetRoles_RoleId FOREIGN KEY(RoleId) REFERENCES AspNetRoles(Id) ON DELETE CASCADE " +
                         "   ); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS AspNetRoles ( " +
                        "   Id    TEXT NOT NULL, " +
                        "   Name  TEXT, " +
                        "   NormalizedName    TEXT, " +
                        "   ConcurrencyStamp  TEXT, " +
                        "   CONSTRAINT PK_AspNetRoles PRIMARY KEY(Id) " +
                        "   );" ; 

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS AspNetUserClaims ( " +
                     "   Id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                     "   UserId    TEXT NOT NULL, " +
                     "  ClaimType TEXT, " +
                     "  ClaimValue    TEXT, " +
                     "  CONSTRAINT FK_AspNetUserClaims_AspNetUsers_UserId FOREIGN KEY(UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE " +
                     "   ); ";
                    

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS AspNetUserClaims ( " +
                     "   Id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                     "   UserId    TEXT NOT NULL, " +
                     "  ClaimType TEXT, " +
                     "  ClaimValue    TEXT, " +
                     "  CONSTRAINT FK_AspNetUserClaims_AspNetUsers_UserId FOREIGN KEY(UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE " +
                     "   ); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS AspNetUserLogins ( " +
                    "    LoginProvider TEXT NOT NULL, " +
                       " ProviderKey   TEXT NOT NULL, " +
                       " ProviderDisplayName   TEXT, " +
                       " UserId    TEXT NOT NULL, " +
                       " CONSTRAINT PK_AspNetUserLogins PRIMARY KEY(LoginProvider,ProviderKey), " +
                       " CONSTRAINT FK_AspNetUserLogins_AspNetUsers_UserId FOREIGN KEY(UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE " +
                    " ); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS AspNetUserRoles ( " +
                    "    UserId    TEXT NOT NULL, " +
                       " RoleId    TEXT NOT NULL, " +
                       " CONSTRAINT PK_AspNetUserRoles PRIMARY KEY(UserId,RoleId), " +
                       " CONSTRAINT FK_AspNetUserRoles_AspNetRoles_RoleId FOREIGN KEY(RoleId) REFERENCES AspNetRoles(Id) ON DELETE CASCADE, " +
                       " CONSTRAINT FK_AspNetUserRoles_AspNetUsers_UserId FOREIGN KEY(UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE " +
                    "); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    "EXISTS AspNetUserTokens ( " +
                    "    UserId    TEXT NOT NULL, " +
                       " LoginProvider TEXT NOT NULL, " +
                       " Name  TEXT NOT NULL, " +
                       " Value TEXT, " +
                       " CONSTRAINT PK_AspNetUserTokens PRIMARY KEY(UserId,LoginProvider,Name), " +
                       " CONSTRAINT FK_AspNetUserTokens_AspNetUsers_UserId FOREIGN KEY(UserId) REFERENCES AspNetUsers(Id) ON DELETE CASCADE " +
                    ");";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                                "EXISTS AspNetUsers ( " +
                            "    Id    TEXT NOT NULL, " +
                               " UserName  TEXT, " +
                               " NormalizedUserName    TEXT, " +
                               " Email TEXT, " +
                               " NormalizedEmail   TEXT, " +
                               " EmailConfirmed    INTEGER NOT NULL, " +
                               " PasswordHash  TEXT, " +
                               " SecurityStamp TEXT, " +
                               " ConcurrencyStamp  TEXT, " +
                               " PhoneNumber   TEXT, " +
                               " PhoneNumberConfirmed  INTEGER NOT NULL, " +
                               " TwoFactorEnabled  INTEGER NOT NULL, " +
                               " LockoutEnd    TEXT, " +
                               " LockoutEnabled    INTEGER NOT NULL, " +
                               " AccessFailedCount INTEGER NOT NULL, " +
                               " CONSTRAINT PK_AspNetUsers PRIMARY KEY(Id) " +
                            "); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                        " EXISTS Copyright( " +
                        " Id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                        " CompanyName   TEXT, " +
                        " YearOfCopy    INTEGER DEFAULT 0, " +
                        " LicenseType   TEXT, " +
                        " Address   TEXT, " +
                        " Telephone TEXT, " +
                        " Email TEXT " +
                        " ); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    " EXISTS Menu( " +
                    "    ID    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "    Title TEXT NOT NULL, " +
                    "    Description   TEXT, " +
                    "    Icon  TEXT, " +
                    "    Status    INTEGER NOT NULL DEFAULT 1, " +
                    "    Link  TEXT, " +
                    "    LinkType  INTEGER NOT NULL DEFAULT 0, " +
                    "    ParentID  INTEGER , " +
                    "    CONSTRAINT FKParentMenu FOREIGN KEY(ParentID) REFERENCES Menu(ID) " +
                    "); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    " EXISTS MultiChoice(" +
                    "    ID    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "    Topic TEXT NOT NULL, " +
                    "    Category  TEXT, " +
                    "    CreatedDate   timestamp, " +
                    "    StartDate timestamp, " +
                    "    EndDate   timestamp, " +
                    "    Status    INTEGER NOT NULL DEFAULT 1, " +
                    "    Position  NONE NOT NULL DEFAULT 1, " +
                    "    QuestionType  INTEGER NOT NULL DEFAULT 0 " +
                    "); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();

                    tableCommand = "CREATE TABLE IF NOT " +
                    " EXISTS Question( " +
                    "    ID    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +
                    "    Title TEXT, " +
                    "    Content   TEXT NOT NULL, " +
                    "    Status    INTEGER NOT NULL DEFAULT 1, " +
                    "    QType INTEGER NOT NULL DEFAULT 0, " +
                    "    Point INTEGER NOT NULL DEFAULT 1, " +
                    "    QuestionGroupID   INTEGER NOT NULL, " +
                    "    CONSTRAINT FKQuestionCate FOREIGN KEY(QuestionGroupID) REFERENCES MultiChoice(ID) " +
                    "); ";

                    createTable = new SqliteCommand(tableCommand, db);
                    createTable.ExecuteReader();
                }
            }
#pragma warning disable CS0168 // The variable 'ex' is declared but never used
            catch (Exception ex) { }
#pragma warning restore CS0168 // The variable 'ex' is declared but never used
        }
        public static string DatabaseName()
        {
            string database_name = XMLConfig_getValue("database_name");
            return Path.Combine(ApplicationData.Current.LocalFolder.Path, database_name);
        }
        public static string Url_WebApi()
        {
            return XMLConfig_getValue("url_webapi");
        }
        public static bool OnlineMode()
        {
            bool isMode = false;
            string mode = XMLConfig_getValue("onlinemode");
            if(mode.Equals("true"))
            {
                isMode = true;
            }
            return isMode;
        }
        public static string XMLConfig_getValue(string key)
        {
            DataSet dsResult = new DataSet();
            string XMLFilePath = Path.Combine(Package.Current.InstalledLocation.Path, "XmlConfig.xml");
            XmlReader loadedData = XmlReader.Create(XMLFilePath);
            dsResult.ReadXml(loadedData);
            if (dsResult.Tables.Contains("add"))
            {
                DataTable dtResult = dsResult.Tables["add"];
                for(int i = 0;i < dtResult.Rows.Count;i++)
                {
                    if(dtResult.Rows[i]["key"].ToString() == key)
                    {
                        return dtResult.Rows[i]["value"].ToString();
                    }    
                }    
            }
            return "";
        }
        public static obj_Table list_TableContains_synchronized()
        {
            obj_Table obj = new obj_Table();
            List<obj_Row> lObj = new List<obj_Row>();
            obj_Row objR = null;
            string table_synchronized = XMLConfig_getValue("table_synchronized");
            string[] arrTable = table_synchronized.Split(",");
            foreach(var o in arrTable)
            {
                objR = new obj_Row();
                objR.Value = o.Trim().ToLower();
                lObj.Add(objR);
            }
            obj.table_name = lObj;
            return obj;
        }
        public static string SyncDB()
        {
            List<obj_Row> entries = new List<obj_Row>();
            obj_Table lTable = list_TableContains_synchronized();
            string jsonObj = JsonConvert.SerializeObject(lTable);
            var content = new StringContent(jsonObj, System.Text.Encoding.UTF8, "application/json");
            string url_webapi = XMLConfig_getValue("url_webapi");
            HttpClient client = new HttpClient();
            content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            var resp = client.PostAsync(url_webapi + "Home/DbSynchronized", content).Result;
            string result = resp.Content.ReadAsStringAsync().Result;
            if (!string.IsNullOrEmpty(result))
            {
                entries = JsonConvert.DeserializeObject<List<obj_Row>>(result);
                string dbpath = CoreConfig.DatabaseName();
                DataBLL.SyncDB_BLL(entries, lTable, dbpath);
            }
            return "";
        }


    }
}
