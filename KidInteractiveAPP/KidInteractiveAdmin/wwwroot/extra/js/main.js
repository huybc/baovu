// ======Sticky===========
window.onscroll = function() {
    myFunction()
};
var header = document.getElementById("myHeader");
var sticky = header.offsetTop;

function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

// ======Home=full-pages=======
//using document ready...
$(document).ready(function() {

    //initialising fullpage.js in the jQuery way
    $('#fullpage').fullpage({
        sectionsColor: ['#ffffff', '#ffffff', '#0e0e2d1a', '#ffffff', '#0a0a271a', '#0e0e2d', '#ffffff'],
        navigation: true,
        slidesNavigation: true,
    });

    // calling fullpage.js methods using jQuery
    $('#moveSectionUp').click(function(e) {
        e.preventDefault();
        $.fn.fullpage.moveSectionUp();
    });

    $('#moveSectionDown').click(function(e) {
        e.preventDefault();
        $.fn.fullpage.moveSectionDown();
    });
});

// ==========slide-product===========
$(document).ready(function() {
    $('.slide-product').slick({
        centerMode: true,
        autoplay: true,
        arrows: false,
        // centerPadding: '60px',
        slidesToShow: 3,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                centerMode: true,
                // centerPadding: '40px',
                slidesToShow: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                arrows: false,
                centerMode: true,
                // centerPadding: '40px',
                slidesToShow: 1
            }
        }]
    });
    $('.next-2').click(function() {
        $(".slide-product").slick('slickNext');
    });
    $('.pre-2').click(function() {
        $(".slide-product").slick('slickPrev');
    });
});
// =========tab================
$(document).ready(function() {
        $('ul.tabs li').click(function() {
            var tab_id = $(this).attr('data-tab');
            $('ul.tabs li').removeClass('current');
            $('.tab-content').removeClass('current');
            $(this).addClass('current');
            $("#" + tab_id).addClass('current');
        });
    })
    // ==========Home=Slide-news-------
$(document).ready(function() {
    $('.slide-news').slick({

        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        infinite: true,
        dots: false,

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.next-3').click(function() {
        $(".slide-news").slick('slickNext');
    });
    $('.pre-3').click(function() {
        $(".slide-news").slick('slickPrev');
    });
});
// ==========Slide-blog=========
$(document).ready(function() {
    $('.slide-videos').slick({

        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: false,
        infinite: true,
        dots: false,

        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
});