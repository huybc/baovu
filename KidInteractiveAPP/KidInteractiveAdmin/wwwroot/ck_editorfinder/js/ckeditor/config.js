/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript';
    config.extraPlugins = 'justify';

    //config.filebrowserBrowseUrl = '/ckfinder1/ckfinder.html?type=Files';
    //config.filebrowserImageBrowseUrl = '/ckfinder1/ckfinder.html?type=Images';
    //config.filebrowserUploadUrl = '/ckfinder1/core/connector/php/connector.php?command=QuickUpload&type=Files';
    //config.filebrowserImageUploadUrl = '/ckfinder1/core/connector/php/connector.php?command=QuickUpload&type=Images';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    config.contentsCss = [CKEDITOR.getUrl('mobirise-icons.css'), '/assets/web/assets/mobirise-icons/mobirise-icons.css'];
    config.contentsCss = [CKEDITOR.getUrl('tether.min.css'), '/assets/tether/tether.min.css'];
    config.contentsCss = [CKEDITOR.getUrl('socicon.min.css'), '/assets/socicon/css/socicon.min.css'];
    config.contentsCss = [CKEDITOR.getUrl('bootstrap.min.css'), '/assets/bootstrap/css/bootstrap.min.css'];
    config.contentsCss = [CKEDITOR.getUrl('style.css'), '/assets/puritym/css/style.css'];
    config.contentsCss = [CKEDITOR.getUrl('style.light.css'), '/assets/dropdown-menu/style.light.css'];
    config.contentsCss = [CKEDITOR.getUrl('mbr-additional.css'), '/assets/mobirise/css/mbr-additional.css'];
};
