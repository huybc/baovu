﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.
$('#mediaConfiguration').off('click').on('click', function () {
    $.ajax({
        url: "/MediaManagement/Check",
        type: "get",
        success: function (data) {
            if (Number(data) === 1) {
                CKFinder.popup({
                    configPath: '/ckfinder/ckfinder.html?type=Images',
                    resourceType: 'Media'
                });
            }
            else if (Number(data) === 2){
                window.location.href = '/Identity/Account/Login';
            }
        }
    });

});
// Write your JavaScript code.
	var num = 50; //number of pixels before modifying styles
		$(window).bind('scroll', function () {
			if ($(window).scrollTop() > num) {
				$('#top-menu').addClass('fixed');
			} else {
				$('#top-menu').removeClass('fixed');
			}
		});