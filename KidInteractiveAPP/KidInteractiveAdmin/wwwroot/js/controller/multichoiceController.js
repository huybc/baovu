﻿R.MultiChoice = {
    Init: function () {
        $(document).ready(function () {
            $('#multi_table').DataTable();
        });
        
        R.MultiChoice.RegisterEvents();
        R.MultiChoice.Api = R.GetApi();
        R.MultiChoice.CountQues = 1;
        R.MultiChoice.CountAns = 1;
    },
    RegisterEvents: function () {
        $('.modify').off('click').on('click', function () {
            var id = $(this).data('id');
            R.MultiChoice.GetDetails(id);
        });
        $('.delete').off('click').on('click', function () {
            var isDelete;
            var r = confirm("Ban co chac chan muon xoa khong?")
            if (r == true) {
                var id = $(this).data('id');
                R.MultiChoice.DeleteMultichoice(id);
            }
        })
        $('#add_multichoice').off('click').on('click', function () {
            var id = 0;
            R.MultiChoice.GetDetails(id);
        });
        $('._clone_question').off('click').on('click', function () {
            console.log(1);
            var template = $('.question-item').first();
            var target = $('._binding_question_item');
            target.append("<hr />")
            template.clone().appendTo(target);
            //R.MultiChoice.CloneElement(template, target);
        })
        $('.save-multichoice-detail').off('click').on('click', function () {
            console.log(1);
            R.MultiChoice.Save();
        })
        $('.btn-clone-ans').off('click').on('click', function () {
            console.log(1);
            var template = $('.answer').first();
            var target = $('.clone_answers');
            target.append("<hr />");
            var cl = template.clone();
            dnm_name.appendTo(target);
            cl.find(ans).attr('name', 'ans_detail_' + CountAns)
            R.MultiChoice.CountAns++;
        })
    },
    SetCkeditor: function () {



        CKEDITOR.replace('quesion_detail_0', {
            filebrowserBrowseUrl: config_filebrowserBrowseUrl,
            filebrowserImageBrowseUrl: config_filebrowserImageBrowseUrl,
            filebrowserUploadUrl: config_filebrowserUploadUrl,
            filebrowserImageUploadUrl: config_filebrowserImageUploadUrl
        });
        CKEDITOR.replace("ans_detail_0", {
            filebrowserBrowseUrl: config_filebrowserBrowseUrl,
            filebrowserImageBrowseUrl: config_filebrowserImageBrowseUrl,
            filebrowserUploadUrl: config_filebrowserUploadUrl,
            filebrowserImageUploadUrl: config_filebrowserImageUploadUrl
        });
    },
    GetDetails: function (id) {

        var url = "/MultiChoice/GetDetail?id=" + id + "";
        $.get(url, function (response) {
            //console.log(response);
            $('#save_multichoice').modal('show');
            $('#save_multichoice').find('.modal-body').data('id', id);
            $('#save_multichoice').find('.modal-body').html('').html(response);
            R.MultiChoice.SetCkeditor();
            R.MultiChoice.RegisterEvents();
            
        })
    },
    CloneElement: function (template, target) {
        var tar = $(target);
        tar.append(template);

    },
    Save: function () {
        tieu_de = $('._mul_tieu_de').val();
        danh_muc = $('._mul_danh_muc').val();
        order = $('._mul_order').val();
        bat_dau = $('._mul_bat_dau').val();
        ket_thuc = $('._mul_ket_thuc').val();
        trang_thai = $('._mul_trang_thai').val();
        phan_loai = $('._mul_phan_loai').val();

        var multi = {
            Id: $('.multichoice_info').data('id'),
            Topic: tieu_de,
            Category: danh_muc,
            CreatedDate: 1592845577,
            StartDate: 1592845577,
            EndDate: 1592855577,
            Status: trang_thai,
            Position: "MzMtMzI=",
            QuestionType: phan_loai,
        }
        var questions = [];
        $('.question-item').each(function (element) {
            var content = $(this).find('.question_detail').val();
            var Question = {
                Id: $(this).data('id'),
                Title: content,
                Content: content,
                Status: 1,
                Qtype: 1,
                Point: 10,
                QuestionGroupId: 0
            };
            var question = {
                Question: Question,
                Answers: []
            }
            var answers = [];
            $(this).find('.answer').each(function (elm) {
                var answer = {
                    Id: $(this).data('id'),
                    Content: $(this).find('.ans').val(),
                    IsCorrect: $(this).find('.isTrue').is(':checked') == true ? 1 : 0,
                    Status: 1,
                    QuestionId: 0,
                }
                answers.push(answer);
            })
            question.Answers = answers;

            questions.push(question);
        })
        var params = {
            Multi: multi,
            QuestionDetails: questions
        }
        console.log(params);
        var url = "/MultiChoice/SaveMultichoice";
        $.post(url, params, function (response) {
            console.log(response);
            alert("Luu thanh cong")
            //location.reload(true);
            R.MultiChoice.Refresh();
            R.MultiChoice.RegisterEvents();
        })



    },
    DeleteMultichoice: function (id) {
        var url = "/MultiChoice/DeleteMultichoice?id=" + id + "";
        $.get(url, function (response) {
            console.log(response);
            R.MultiChoice.Refresh();
            R.MultiChoice.RegisterEvents();
        })
    },
    Refresh: function () {
        var url = "/MultiChoice/RefreshPage";
        $.get(url, function (response) {
            console.log(response);
            $('#multi_table').html('').html(response);
            $('#multi_table').DataTable();
            R.MultiChoice.RegisterEvents();
        })
    }

}
$(function () {
    R.MultiChoice.Init();
})