﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using KidInteractiveAdmin.Models;
using Microsoft.AspNetCore.Razor.Language;
using DataAccessLibrary.Models;
using DataAccessLibrary.DataAccess_Online;
using System.Net;
using System.Text.RegularExpressions;

namespace KidInteractiveAdmin.Controllers
{
    public class HomeController : Controller
    {
        private string _api = "http://206.189.82.108:9119/";
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult VideoView(int id)
        {
            var body = "";
            var b = Menu_On.GetData(_api).Where(r => r.Id == id).FirstOrDefault();
            if (b != null)
            {
                body = b.Link;
            }
            ViewBag.Link = body;
            //Console.WriteLine(RenderStory(body));
            return View();
        }
        public IActionResult StoryView(int id) 
        {
            //Xu ly body
            //get
            var body = "";
            var b = Menu_On.GetData(_api).Where(r => r.Id == id).FirstOrDefault();
            if(b != null) {
                body = b.Description;
            }
            ViewBag.Body = RenderStory(body);
            Console.WriteLine(RenderStory(body));
            return View();
        }
        public IActionResult PictureView(int id)
        {
            var body = "";
            var b = Menu_On.GetData(_api).Where(r => r.Id == id).FirstOrDefault();
            if(b != null)
            {
                body = b.Description;
                var res = RenderPicture(body);
                return View(res);
            }
            return BadRequest();
        }
        public IActionResult GameView(int id)
        {
            var body = "";
            var b = Menu_On.GetData(_api).Where(r => r.Id == id).FirstOrDefault();
            if (b != null)
            {
                body = b.Link;
                return Ok(body);
            }
            return BadRequest();
        }
        public IActionResult FunKeyboard()
        {
            return View();
        }
        public IActionResult FunPiano()
        {
            return View();
        }
        

        public IActionResult MultichoiceIndex()
        {
            var model = DataAccessLibrary.DataAccess_Online.MultiChoice_On.GetListMultiChoice(_api);
            
            return View(model);
        }
        public IActionResult MenuIndex()
        {
            var model = DataAccessLibrary.DataAccess.Menu_GetData(true, "", _api);
            var lst_build_tree = new List<ItemInfo>();
            foreach(var item in model)
            {
                var r = new ItemInfo();
                r.Id = (int)item.Id;
                r.ParentId = item.ParentId == null ? 0 : (int)item.ParentId;
                r.Name = item.Title;
                lst_build_tree.Add(r);
            }
            var result = GetTree(lst_build_tree, 0);
                
            return View(result);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private List<Tree> GetTree(List<ItemInfo> list, int parent)
        {
            return list.Where(x => x.ParentId == parent).Select(x => new Tree
            {
                Id = x.Id,
                Name = x.Name,
                Root = parent,
                List = GetTree(list, x.Id)
            }).ToList();
        }
        private string RenderStory(string body)
        {
            
            body = body.Replace("[", "<div class=\"page-item\">").Replace("]", "</div>");
            return body;
        }
        private List<string> RenderPicture(string body)
        {
            var result = new List<string>();
            
            if(!string.IsNullOrEmpty(body))
            {
                string pattern = @"src=""(.*?)""";
                string input = body;
                RegexOptions options = RegexOptions.Multiline;

                foreach (Match m in Regex.Matches(input, pattern, options))
                {
                    Console.WriteLine(m.Groups[1].Value);
                    result.Add(m.Groups[1].Value);
                }
            }
            return result;
        }

    }
    public class ItemInfo
    {
        public int Id { get; set; }
        public int ParentId { get; set; }
        public string Name { get; set; }
        public Menu Parent { get; set; }
        public List<Menu> Childs { get; set; }
    }
    public class Tree
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Root { get; set; }
        public List<Tree> List { get; set; }
    }
}
