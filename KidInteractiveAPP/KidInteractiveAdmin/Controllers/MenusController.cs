﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DataAccessLibrary.Models;

using DataAccessLibrary;

namespace KidInteractiveAdmin.Controllers
{
    public class MenusController : Controller
    {
        private string api = "http://206.189.82.108:9119/";
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        //[Route("Detail/{id}")]
        public IActionResult GetData(int id)
        {
            var a = DataAccessLibrary.DataAccess_Online.Menu_On.GetData(api);
            return Ok(a);
        }
        [HttpPost]
        //[Route("Detail/{id}")]
        public IActionResult PostData(Menu oMenu)
        {
            string message = string.Empty;
            if (oMenu.ParentId == -1)
                oMenu.ParentId = null;
            if(oMenu.Id == 0)
            {
                message = DataAccess.Menu_InsertData(true, "", api, oMenu);
            }
            else
            {
                message = DataAccess.Menu_UpdateData(true, "", api, oMenu);
            }
            return Ok(message);
        }
        public IActionResult DeleteData(string id)
        {
            string message = DataAccess.Menu_DeleteData(true, "", api, id);
            return Ok(message);
        }
    }
}